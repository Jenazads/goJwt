package gojwt

import "fmt";

func init() {
  fmt.Println("\n\n====> goJwt (GoJweto) version 2.0.1 last update 30/03/2018 \n")
  fmt.Println("====> Init the Gojwt (GoJweto) Library for JSON Web Token developed in Golang :D\n")
  fmt.Println("====> by Jenazads.\n\n")
}
